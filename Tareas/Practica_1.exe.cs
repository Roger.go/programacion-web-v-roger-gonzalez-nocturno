using System;

class Program
{
    static void Main()
    {
        Console.WriteLine("Ingrese el nombre:");
        string nombre = Console.ReadLine();

        Console.WriteLine("Ingrese la edad:");
        int edad = int.Parse(Console.ReadLine());

        Console.WriteLine("Ingrese el salario bruto:");
        double salarioBruto = double.Parse(Console.ReadLine());

        // Calcular los descuentos
        double descuentoSeguro = salarioBruto * 0.0725;
        double descuentoSeguroEducativo = salarioBruto * 0.0125;
        double salarioDescontado = salarioBruto - descuentoSeguro - descuentoSeguroEducativo;

        // Imprimir la tabla
        Console.WriteLine("\nResultados:");
        Console.WriteLine("=======================================");
        Console.WriteLine("|   Nombre   |   Edad   |   Salario Bruto   |   Salario Descontado   |");
        Console.WriteLine("=======================================");
        Console.WriteLine($"|   {nombre,-10}   |   {edad,-5}   |   {salarioBruto,-15:C}   |   {salarioDescontado,-22:C}   |");
        Console.WriteLine("=======================================");
    }
}
